/*
 * Class to model the fluid
 */
package physicsca;

/**
 *
 * @author James Farrell and Kristine Saroka
 */
public class Fluid {
    
    private double density;
    private double viscosity;
    

    public Fluid() {
    }

    public Fluid(double density, double viscosity) {
        this.density = density;
        this.viscosity = viscosity;
    }

    public double getDensity() {
        return density;
    }
    
    public double getViscosity() {
        return viscosity;
    }
    
    public void setDensity(double density) {
        this.density = density;
    }
    
    public void setViscosity(double viscosity) {
        this.viscosity = viscosity;
    } 
}