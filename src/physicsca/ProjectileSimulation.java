/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package physicsca;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author James Farrel and Kristine Saroka
 */
public class ProjectileSimulation {

    //Useful for retreiving coordinates
    private final int Z = 2;

    //Scanners
    private static final Scanner NUM_SC = new Scanner(System.in);

    public ProjectileSimulation() {
    }

    public enum Materials {
        WOOD, GLASS, METAL, PLASTIC, EXIT
    };

    public enum Fluids {
        SYRUP, OIL, WATER, HELIUM, EXIT
    };

    public static void main(String[] args) {
        ProjectileSimulation proSim = new ProjectileSimulation();
        proSim.initializeSimulation();
    }

    private void initializeSimulation() {

        ArrayList<Sphere> spheres = new ArrayList<>();
        ArrayList<Fluid> fluids = new ArrayList<>();

        double radius = 0.05;
        double objectDensity = 7800.0;

        double[] fluidProperties;
        double fluidDensity = 80;
        double viscosity = 0.0089;

        double[] position = {-3.0, 2.0, 5.0};
        double[] velocity = {-5.0, 14.0, 2.0};
        double[] angular = {-3.3333333333333335, -1.6666666666666667, 3.3333333333333335};
        double[] wind = {2.0, 3.0, 0.0};

        double fps = 60;
        double t = 3.5;

        double gravity = 9.81;
        double Cd = 0.1;

        int input = promptUserForInput("\nDo you wish to input values or load values?\n1: Input\n2: Load\n0: Exit\n : ", 0, 2);
        if (input == 1) {

            //Fluid Properties
            fluidProperties = openFluidMenu();
            fluidDensity = fluidProperties[0];
            viscosity = fluidProperties[1];
            wind = promptUserForInitialConditions("\nPlease enter a flowing speed (-500 - 500) - \n", -500.0, 500.0);

            while (true) {

                //Object Properties
                radius = promptUserForInput("\nPlease enter a radius for the sphere (0.01 - 500): ", 0.01, 500);
                objectDensity = openMaterialMenu();

                //Initial Conditions
                position = promptUserForInitialConditions("\nPlease enter an initial position (radius - 500) - \n", radius, 500);
                velocity = promptUserForInitialConditions("\nPlease enter an initial velocity (-500 - 500) - \n", -500.0, 500.0);
                angular = promptUserForInitialConditions("\nPlease enter an angular velocity (-500 - 500) - \n", -500.0, 500.0);

                //Create Objects
                spheres.add(new Sphere(radius, objectDensity, position, velocity, angular));

                //If the user wishes to quit the program
                if (promptUserForInput("\nDo you wish to simulate another object?\n1: Yes\n0: No\n : ", 0, 1) == 0) {
                    break;
                }
                System.out.println("\nObject added to simulation.");
            }

            //Create objects
            fluids.add(new Fluid(fluidDensity, viscosity));

            //Time Components
            fps = promptUserForInput("\nPlease enter the desired FPS (5 - 120): ", 5, 120);
            t = promptUserForInput("\nPlease enter an initial time (0 - 500): ", 0.0, 500.0);

            //Forces
            gravity = promptUserForInput("\nPlease enter a gravitational force (0 - 100): ", 0.0, 100.0);
            Cd = promptUserForInput("\nPlease enter the coefficient of drag (0 - 1): ", 0.0, 1.0);

        } else {
            spheres.add(new Sphere(radius, objectDensity, position, velocity, angular));
            fluids.add(new Fluid(fluidDensity, viscosity));
        }
        System.out.println("\nSimulation Initializing..");
        Rk4(spheres, fluids, t, wind, fps, gravity, Cd);
    }

    private void Rk4(ArrayList<Sphere> spheres, ArrayList<Fluid> fluids, double t, double[] wind, double fps, double gravity, double Cd) {

        ArrayList<double[]> positions = new ArrayList<>();
        double h = 1 / fps;

        double m;
        double[] Fnet;
        double[] a;
        double[] pn;
        double[] vn;
        double[][] k = new double[2][3];
        double[] k1p = new double[3];
        double[] k1v = new double[3];
        double[] k2p = new double[3];
        double[] k2v = new double[3];
        double[] k3p = new double[3];
        double[] k3v = new double[3];
        double[] k4p = new double[3];
        double[] k4v = new double[3];

        for (int x = 0; x < spheres.size(); x++) {

            //Retreive values
            Sphere sphere = (Sphere) spheres.get(x);
            Fluid fluid = (Fluid) fluids.get(x);

            //Acceleration Components
            m = sphere.getMass();
            Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
            a = calculateAcceleration(m, Fnet);

            //Initial Conditions
            pn = sphere.getPosition();
            vn = sphere.getVelocity();

            while (sphere.getPosition(Z) > sphere.getRadius()) {

                //t = t+h
                t = t + h;

                //k1
                for (int i = 0; i < vn.length; i++) {
                    k1p[i] = (h * vn[i]);
                    k1v[i] = (h * a[i]);

                    k[0][i] = (k1p[i]);
                    k[1][i] = (k1v[i]);
                }

                //(p0, v)0 + k1 / 2
                for (int i = 0; i < vn.length; i++) {
                    k1p[i] = pn[i] + (k1p[i] / 2);
                    k1v[i] = vn[i] + (k1v[i] / 2);
                }

                //Calculate acceleration based on new position and velocity
                sphere.setPosition(k1p);
                sphere.setVelocity(k1v);
                Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
                a = calculateAcceleration(m, Fnet);

                //k2 
                for (int i = 0; i < vn.length; i++) {
                    k2p[i] = (h * k1v[i]);
                    k2v[i] = (h * a[i]);

                    k[0][i] += (2 * k2p[i]);
                    k[1][i] += (2 * k2v[i]);
                }

                //(p, v)0 + k2 / 2
                for (int i = 0; i < vn.length; i++) {
                    k2p[i] = pn[i] + (k2p[i] / 2);
                    k2v[i] = vn[i] + (k2v[i] / 2);
                }

                //Calculate acceleration based on new position and velocity
                sphere.setPosition(k2p);
                sphere.setVelocity(k2v);
                Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
                a = calculateAcceleration(m, Fnet);

                //k3 
                for (int i = 0; i < vn.length; i++) {
                    k3p[i] = (h * k2v[i]);
                    k3v[i] = (h * a[i]);

                    k[0][i] += (2 * k3p[i]);
                    k[1][i] += (2 * k3v[i]);
                }

                //(p, v)0 + k3
                for (int i = 0; i < vn.length; i++) {
                    k3p[i] = pn[i] + k3p[i];
                    k3v[i] = vn[i] + k3v[i];
                }

                //Calculate acceleration based on new position and velocity
                sphere.setPosition(k3p);
                sphere.setVelocity(k3v);
                Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
                a = calculateAcceleration(m, Fnet);

                //k4
                for (int i = 0; i < vn.length; i++) {
                    k4p[i] = (h * k3v[i]);
                    k4v[i] = (h * a[i]);

                    k[0][i] += (k4p[i]);
                    k[1][i] += (k4v[i]);
                }

                //k
                for (int i = 0; i < vn.length; i++) {
                    k[0][i] = k[0][i] / 6.0;
                    k[1][i] = k[1][i] / 6.0;
                }

                //(p, v)0 + k
                for (int i = 0; i < vn.length; i++) {
                    pn[i] += k[0][i];
                    vn[i] += k[1][i];
                }

                //Calculate acceleration based on new position and veloctiy 
                sphere.setPosition(pn);
                sphere.setVelocity(vn);
                Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
                a = calculateAcceleration(m, Fnet);

                //Record the k position at time t
                positions.add(new double[]{t, pn[2]});
            }
            display(positions);
        }
    }

    private void display(ArrayList<double[]> positions) {
        XYSeriesDemo xy = new XYSeriesDemo("Projectile Simulation", positions);
        xy.pack();
        RefineryUtilities.centerFrameOnScreen(xy);
        xy.setVisible(true);
    }

    private double[] calculateForces(Sphere sphere, Fluid fluid, double[] wind, double g, double Cd) {

        //Constants
        final double[] k = {0, 0, 1};

        //Initial Conditions
        double[] R = {-3, -1.5, 3};

        //Gravity Components
        double m = sphere.getMass();

        //Drag Components
        double[] v = sphere.getVelocity();
        double p = fluid.getDensity();
        double A = sphere.getFrontalArea();
        double[] Va = calculateApparentVelocity(v, wind);
        double lengthVa = calculateVectorLength(Va);

        //Magnus Components
        double r = sphere.getRadius();
        double w = calculateVectorLength(R);
        double Cl = calculateCl(r, w, lengthVa);
        double[] directionFm = calculateUnitVector(calculateFmDirection(R, Va));
        double lengthFm = calculateFmLength(p, A, Cl, lengthVa);

        //Vectors
        double[] Fg = calculateFg(m, g, k);
        double[] Fd = calculateFd(Cd, A, lengthVa, Va);
        double[] Fm = calculateFm(lengthFm, directionFm);
        double[] Fnet = calculateFnet(Fg, Fd, Fm);

        return Fnet;
    }

    //        while (sphere.getPosition(Z) > sphere.getRadius()) {
//
//            //tn + 1 = tn + h`+
//            t += h;
//            step++;
//            System.out.println("\ntime: " + t + " step: " + step);
//
//            //k1
//            for (int i = 0; i < v0.length; i++) {
//                kp1[i] = (h * v0[i]);
//                kv1[i] = (h * a[i]);
//
//                k[0][i] = (kp1[i]);
//                k[1][i] = (kv1[i]);
//            }
//
//            //(p0, v)0 + k1 / 2
//            for (int i = 0; i < v0.length; i++) {
//                kp1[i] = p0[i] + (kp1[i] / 2);
//                kv1[i] = v0[i] + (kv1[i] / 2);
//            }
//
//            //Calculate acceleration based on new position and velocity
//            sphere.setPosition(kp1);
//            sphere.setVelocity(kv1);
//            Fnet = calculateForces(sphere, fluid);
//            a = calculateAcceleration(m, Fnet);
//
//            //k2 
//            for (int i = 0; i < v0.length; i++) {
//                kp2[i] = (h * kv1[i]);
//                kv2[i] = (h * a[i]);
//
//                k[0][i] += (2 * kp2[i]);
//                k[1][i] += (2 * kv2[i]);
//            }
//
//            //(p, v)0 + k2 / 2
//            for (int i = 0; i < v0.length; i++) {
//                kp2[i] = p0[i] + (kp2[i] / 2);
//                kv2[i] = v0[i] + (kv2[i] / 2);
//            }
//
//            //Calculate acceleration based on new position and velocity
//            sphere.setPosition(kp2);
//            sphere.setVelocity(kv2);
//            Fnet = calculateForces(sphere, fluid);
//            a = calculateAcceleration(m, Fnet);
//
//            //k3 
//            for (int i = 0; i < v0.length; i++) {
//                kp3[i] = (h * kv2[i]);
//                kv3[i] = (h * a[i]);
//
//                k[0][i] += (2 * kp3[i]);
//                k[1][i] += (2 * kv3[i]);
//            }
//
//            //(p, v)0 + k3
//            for (int i = 0; i < v0.length; i++) {
//                kp3[i] = p0[i] + kp3[i];
//                kv3[i] = v0[i] + kv3[i];
//            }
//
//            //Calculate acceleration based on new position and velocity
//            sphere.setPosition(kp3);
//            sphere.setVelocity(kv3);
//            Fnet = calculateForces(sphere, fluid);
//            a = calculateAcceleration(m, Fnet);
//
//            //k4
//            for (int i = 0; i < v0.length; i++) {
//                kp4[i] = (h * kv3[i]);
//                kv4[i] = (h * a[i]);
//
//                k[0][i] += (kp4[i]);
//                k[1][i] += (kv4[i]);
//            }
//
//            //k
//            for (int i = 0; i < v0.length; i++) {
//                k[0][i] = k[0][i] / 6.0;
//                k[1][i] = k[1][i] / 6.0;
//            }
//
//            //(p, v)0 + k
//            for (int i = 0; i < v0.length; i++) {
//                p0[i] += k[0][i];
//                v0[i] += k[1][i];
//            }
//
//            //Calculate acceleration based on new position and veloctiy 
//            sphere.setPosition(p0);
//            sphere.setVelocity(v0);
//            Fnet = calculateForces(sphere, fluid);
//            a = calculateAcceleration(m, Fnet);
//
//            System.out.println("kp [" + p0[0] + ", " + p0[1] + ", " + p0[2] + "] ");
//            System.out.println("kv [" + v0[0] + ", " + v0[1] + ", " + v0[2] + "] ");
//        }
//    private double[] calculateForces(Sphere sphere, Fluid fluid) {
//
//        //Constants
//        final double COEFFICIENT_DRAG = 0.1;
//        final double GRAVITATIONAL_CONSTANT = 9.81;
//
//        //Initial Conditions
//        double[] wind = {2.0, 3.0, 0.0};
//        double[] R = {-3.335, -1.667, 3.335};
//
//        //Gravity Components
//        double m = sphere.getMass();
//        double g = GRAVITATIONAL_CONSTANT;
//        double[] k = {0, 0, 1};
//
//        //Drag Components
//        double[] v = sphere.getVelocity();
//        double p = fluid.getDensity();
//        double A = sphere.getFrontalArea();
//        double Cd = COEFFICIENT_DRAG;
//        double[] Va = calculateApparentVelocity(v, wind);
//        double lengthVa = calculateVectorLength(Va);
//
//        //Magnus Components
//        double r = sphere.getRadius();
//        double w = calculateVectorLength(R);
//        double Cl = calculateCl(r, w, lengthVa);
//        double[] directionFm = calculateUnitVector(calculateFmDirection(R, Va));
//        double lengthFm = calculateFmLength(p, A, Cl, lengthVa);
//
//        //Vectors
//        double[] Fg = calculateFg(m, g, k);
//        double[] Fd = calculateFd(Cd, A, lengthVa, Va);
//        double[] Fm = calculateFm(lengthFm, directionFm);
//        double[] Fnet = calculateFnet(Fg, Fd, Fm);
//
//        return Fnet;
//    }
//        while (sphere.getPosition(Z) > sphere.getRadius()) {
//
//            System.out.println("t: ");
//            System.out.println("pn: " + pn[0]);
//            
//            //tn + 1 = tn + h
//            t += h;
//
//            //k1
//            for (int i = 0; i < vn.length; i++) {
//                k1p[i] = (h * vn[i]);
//                k1v[i] = (h * a[i]);
//
//                k[0][i] = (k1p[i]);
//                k[1][i] = (k1v[i]);
//            }
//
//            //(p0, v)0 + k1 / 2
//            for (int i = 0; i < vn.length; i++) {
//                k1p[i] = pn[i] + (k1p[i] / 2);
//                k1v[i] = vn[i] + (k1v[i] / 2);
//            }
//
//            //Calculate acceleration based on new position and velocity
//            sphere.setPosition(k1p);
//            sphere.setVelocity(k1v);
//            Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
//            a = calculateAcceleration(m, Fnet);
//
//            //k2 
//            for (int i = 0; i < vn.length; i++) {
//                k2p[i] = (h * k1v[i]);
//                k2v[i] = (h * a[i]);
//
//                k[0][i] += (2 * k2p[i]);
//                k[1][i] += (2 * k2v[i]);
//            }
//
//            //(p, v)0 + k2 / 2
//            for (int i = 0; i < vn.length; i++) {
//                k2p[i] = pn[i] + (k2p[i] / 2);
//                k2v[i] = vn[i] + (k2v[i] / 2);
//            }
//
//            //Calculate acceleration based on new position and velocity
//            sphere.setPosition(k2p);
//            sphere.setVelocity(k2v);
//            Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
//            a = calculateAcceleration(m, Fnet);
//
//            //k3 
//            for (int i = 0; i < vn.length; i++) {
//                k3p[i] = (h * k2v[i]);
//                k3v[i] = (h * a[i]);
//
//                k[0][i] += (2 * k3p[i]);
//                k[1][i] += (2 * k3v[i]);
//            }
//
//            //(p, v)0 + k3
//            for (int i = 0; i < vn.length; i++) {
//                k3p[i] = pn[i] + k3p[i];
//                k3v[i] = vn[i] + k3v[i];
//            }
//
//            //Calculate acceleration based on new position and velocity
//            sphere.setPosition(k3p);
//            sphere.setVelocity(k3v);
//            Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
//            a = calculateAcceleration(m, Fnet);
//
//            //k4
//            for (int i = 0; i < vn.length; i++) {
//                k4p[i] = (h * k3v[i]);
//                k4v[i] = (h * a[i]);
//
//                k[0][i] += (k4p[i]);
//                k[1][i] += (k4v[i]);
//            }
//
//            //k
//            for (int i = 0; i < vn.length; i++) {
//                k[0][i] = k[0][i] / 6.0;
//                k[1][i] = k[1][i] / 6.0;
//            }
//
//            //(p, v)0 + k
//            for (int i = 0; i < vn.length; i++) {
//                pn[i] += k[0][i];
//                vn[i] += k[1][i];
//            }
//
//            //Calculate acceleration based on new position and veloctiy 
//            sphere.setPosition(pn);
//            sphere.setVelocity(vn);
//            Fnet = calculateForces(sphere, fluid, wind, gravity, Cd);
//            a = calculateAcceleration(m, Fnet);
//        }
//    private double[] calculateForces(Sphere sphere, Fluid fluid, double[] wind, double g, double Cd) {
//
//        //Constants
//        final double[] k = {0, 0, 1};
//
//        //Initial Conditions
//        double[] R = {-3, -1.5, 3};
//
//        //Gravity Components
//        double m = sphere.getMass();
//
//        //Drag Components
//        double[] v = sphere.getVelocity();
//        double p = fluid.getDensity();
//        double A = sphere.getFrontalArea();
//        double[] Va = calculateApparentVelocity(v, wind);
//        double lengthVa = calculateVectorLength(Va);
//
//        //Magnus Components
//        double r = sphere.getRadius();
//        double w = calculateVectorLength(R);
//        double Cl = calculateCl(r, w, lengthVa);
//        double[] directionFm = calculateUnitVector(calculateFmDirection(R, Va));
//        double lengthFm = calculateFmLength(p, A, Cl, lengthVa);
//
//        //Vectors
//        double[] Fg = calculateFg(m, g, k);
//        double[] Fd = calculateFd(Cd, A, lengthVa, Va);
//        double[] Fm = calculateFm(lengthFm, directionFm);
//        double[] Fnet = calculateFnet(Fg, Fd, Fm);
//
//        return Fnet;
//    }
    /**
     * Calculate the net force acting on the object
     *
     * @param Fg gravitational force
     * @param Fd drag force
     * @param Fm magnus force
     * @return the net force
     */
    private double[] calculateFnet(double[] Fg, double[] Fd, double[] Fm) {
        double[] fNet = new double[3];

        //Calculate the net force
        for (int i = 0; i < fNet.length; i++) {
            fNet[i] = Fg[i] + Fd[i] + Fm[i];
        }
        return fNet;
    }

    /**
     * Calculate the gravitational force
     *
     * @param m mass of the sphere
     * @param g gravitational constant
     * @param k direction gravity acts in
     * @return gravitational force vector
     */
    private double[] calculateFg(double m, double g, double[] k) {
        double[] Fg = new double[3];
        //Calculare the gravitational force
        for (int i = 0; i < Fg.length; i++) {
            Fg[i] = (m * g * k[i]) * -1;
        }
        return Fg;
    }

    /**
     * Calculate the drag force
     *
     * @param Cd coefficient of drag
     * @param A frontal area
     * @param lengthVa apparent velocity length
     * @param Va apparent velocity
     * @return drag force vector
     */
    private double[] calculateFd(double Cd, double A, double lengthVa, double[] Va) {
        double[] Fd = new double[3];

        //Calculate the drag force
        for (int i = 0; i < Fd.length; i++) {
            Fd[i] = (0.5) * Cd * A * lengthVa * Va[i];
        }
        return Fd;
    }

    /**
     * Calculate the magnus force
     *
     * @param lengthFm
     * @param directionFm
     * @return magnus force vector
     */
    private double[] calculateFm(double lengthFm, double[] directionFm) {
        double[] Fm = new double[3];

        //Calculate the magnus force
        for (int i = 0; i < Fm.length; i++) {
            Fm[i] = lengthFm * directionFm[i];
        }
        return Fm;
    }

    /**
     * Calculate the apparent velocity
     *
     * @param v velocity vector
     * @param wind velocity of wind
     * @return apparent velocity
     */
    private double[] calculateApparentVelocity(double[] v, double[] wind) {
        double[] Va = new double[3];

        //Calculate the apparent velocity
        for (int i = 0; i < Va.length; i++) {
            Va[i] = v[i] - wind[i];
        }
        return Va;
    }

    /**
     * Calculate the lift coefficient
     *
     * @param r radius of the sphere
     * @param w omega - length of the spin vector
     * @param lengthVa length of apparent velocity
     * @return lift coefficient
     */
    public double calculateCl(double r, double w, double lengthVa) {
        return (r * w) / lengthVa;
    }

    /**
     * Calculate the magnus force magnitude
     *
     * @param p density of the fluid
     * @param A fontal area of the sphere
     * @param Cl lift coefficient
     * @param lengthVa length of apparent velocity
     * @return magnitude of the magnus force
     */
    private double calculateFmLength(double p, double A, double Cl, double lengthVa) {
        return (0.5) * p * A * Cl * Math.pow(lengthVa, 2);

    }

    /**
     * Calculate the direction the magnus force is acting in
     *
     * @param R spin vector
     * @param Va apparent velocity vector
     * @return direction magnus force is acting in
     */
    private double[] calculateFmDirection(double[] R, double[] Va) {
        double[] FmD = new double[3];
        FmD[0] = (R[1] * Va[2]) - (R[2] * Va[1]);
        FmD[1] = (R[2] * Va[0]) - (R[0] * Va[2]);
        FmD[2] = (R[0] * Va[1]) - (R[1] * Va[0]);
        return FmD;
    }

    /**
     * Calculate the spheres acceleration
     *
     * @param mass
     * @param Fnet
     * @return the acceleration of the sphere
     */
    private double[] calculateAcceleration(double m, double[] Fnet) {
        double[] acceleration = new double[3];

        //Calculate the accelleration
        for (int i = 0; i < acceleration.length; i++) {
            acceleration[i] = (Fnet[i] / m);
        }
        return acceleration;
    }

    /**
     * Calculate the length of a given vector
     *
     * @param vector
     * @return length of vector
     */
    private double calculateVectorLength(double[] vector) {
        double total = 0;
        for (int i = 0; i < vector.length; i++) {
            total += Math.pow(vector[i], 2d);
        }
        return Math.sqrt(total);
    }

    /**
     * Calculate the unit vector
     *
     * @param vector
     * @return unit vector
     */
    public double[] calculateUnitVector(double[] vector) {
        double vectorLength = calculateVectorLength(vector);
        for (int i = 0; i < vector.length; i++) {
            vector[i] = (vector[i] / vectorLength);
        }
        return vector;
    }

    /**
     *
     * @return
     */
    public double[] openFluidMenu() {
        int MIN = 0;
        int MAX = 4;

        //Density
        final double SYRUP_DENSITY = 1;
        final double OIL_DENSITY = 1;
        final double WATER_DENSITY = 1;
        final double HELIUM_DENSITY = 1;

        //Viscosity
        final double SYRUP_VISCOSITY = 1;
        final double OIL_VISCOSITY = 1;
        final double WATER_VISCOSITY = 1;
        final double HELIUM_VISCOSITY = 1;

        double[] fluidProperties = new double[2];

        int userInput = promptUserForInput(printFluidMenu(), MIN, MAX);
        Fluids fluid = Fluids.values()[userInput];

        switch (fluid) {
            case SYRUP:
                fluidProperties[0] = SYRUP_DENSITY;
                fluidProperties[1] = SYRUP_VISCOSITY;
                break;

            case OIL:
                fluidProperties[0] = OIL_DENSITY;
                fluidProperties[1] = OIL_VISCOSITY;
                break;

            case WATER:
                fluidProperties[0] = WATER_DENSITY;
                fluidProperties[1] = WATER_VISCOSITY;
                break;

            case HELIUM:
                fluidProperties[0] = HELIUM_DENSITY;
                fluidProperties[1] = HELIUM_VISCOSITY;
                break;

            case EXIT:
                System.out.println("\nGoodbye!");
                System.exit(0);
                break;
        }
        return fluidProperties;
    }

    /**
     *
     * @return
     */
    public double openMaterialMenu() {
        int MIN = 0;
        int MAX = 4;

        double materialDensity = 0;

        final double WOOD_DENSITY = 1;
        final double GLASS_DENSITY = 1;
        final double METAL_DENSITY = 1;
        final double PLASTIC_DENSITY = 1;

        int userInput = promptUserForInput(printMaterialMenu(), MIN, MAX);
        Materials material = Materials.values()[userInput];

        switch (material) {
            case WOOD:
                materialDensity = WOOD_DENSITY;
                break;

            case GLASS:
                materialDensity = GLASS_DENSITY;
                break;

            case METAL:
                materialDensity = METAL_DENSITY;
                break;

            case PLASTIC:
                materialDensity = PLASTIC_DENSITY;
                break;

            case EXIT:
                System.out.println("\nGoodbye!");
                System.exit(0);
                break;
        }
        return materialDensity;
    }

    /**
     *
     * @param prompt
     * @param min
     * @param max
     * @return
     */
    public static int promptUserForInput(String prompt, int min, int max) {
        while (true) {
            System.out.print(prompt);
            if (NUM_SC.hasNextInt()) {
                int input = NUM_SC.nextInt();
                if (input < min || input > max) {
                    System.out.println("\nInvalid entry. Please enter a number "
                            + "between " + min + " and " + max + ".");
                } else {
                    return input;
                }
            } else {
                System.out.println("\nInvalid entry. Please enter a valid number.");
                NUM_SC.next();
            }
        }
    }

    /**
     *
     * @param prompt
     * @param min
     * @param max
     * @return
     */
    public static double promptUserForInput(String prompt, double min, double max) {
        while (true) {
            System.out.print(prompt);
            if (NUM_SC.hasNextDouble()) {
                double input = NUM_SC.nextDouble();
                if (input < min || input > max) {
                    System.out.println("\nInvalid entry. Please enter a number "
                            + "between " + min + " and " + max + ".");
                } else {
                    return input;
                }
            } else {
                System.out.println("\nInvalid entry. Please enter a valid number.");
                NUM_SC.next();
            }
        }
    }

    /**
     *
     * @param prompt
     * @param min
     * @param max
     * @return
     */
    public static double[] promptUserForInitialConditions(String prompt, double min, double max) {
        double[] vector = new double[3];
        String[] output = new String[]{"i: ", "j: ", "k: "};
        while (true) {
            System.out.print(prompt); 
            for (int i = 0; i < vector.length; i++) {
                System.out.print(output[i]);
                if (NUM_SC.hasNextDouble()) {
                    double input = NUM_SC.nextDouble();
                    if (input < min || input > max) {
                        System.out.println("\nInvalid entry. Please enter a number "
                                + "between " + min + " and " + max + ".");
                    } else {
                        vector[i] = input;
                    }
                } else {
                    System.out.println("\nInvalid entry. Please enter a valid condition.");
                    NUM_SC.next();
                }
            }
            return vector;
        }
    }

    /**
     *
     * @return
     */
    private String printMaterialMenu() {
        return "\nPlease select a material: "
                + "\n1: Wood"
                + "\n2: Glass"
                + "\n3: Metal"
                + "\n4: Plastic"
                + "\n0: Exit Program"
                + "\n : ";
    }

    /**
     *
     * @return
     */
    private String printFluidMenu() {
        return "\nPlease select a fluid: "
                + "\n1: Syrup"
                + "\n2: Oil"
                + "\n3: Water"
                + "\n4: Helium"
                + "\n0: Exit Program"
                + "\n : ";
    }

}

//            double[] position = {-3.0, 2.0, 5.0};
//            double[] velocity = {-5.0, 14.0, 2.0};
//            double[] angular = {-3.0, -1.5, 3.0};
//            double[] wind = {2.0, 3.0, 0.0};
//Time Components
//            double t = 0;
//            double fps = 60;
//Forces
//            double Gravity = 9.81;
//            double Cd = 0.1;
//            output.write("Gravitational Constant: 9.81");
//            output.write("Initial Conditions:\n");
//            output.write("Initial Position: [" + position[0] + ", " + position[1] + ", " + position[2] + "]\n");
//            output.write("Initial Velocity: [" + velocity[0] + ", " + velocity[1] + ", " + velocity[2] + "]\n");
//            output.write("Spin Vector: [" + axis[0] + ", " + axis[1] + ", " + axis[2] + "]\n");
//            output.write("\nSphere\n");
//            output.write("Radius: " + r);
//            output.write("Density: " + p);
//            output.write("Mass: " + m);
//            output.write("Frontal Area: " + A);
