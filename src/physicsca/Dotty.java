package physicsca;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Dotty {

    public Dotty(ArrayList<double[]> results) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }

                JFrame frame = new JFrame("Testing");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());
                frame.add(new TestPane(results));
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }

    public class TestPane extends JPanel {

        private int i = 0;
        private BufferedImage background;

        public TestPane(ArrayList<double[]> results) {
            background = new BufferedImage(600, 600, BufferedImage.TYPE_INT_ARGB);
            Timer timer;
            System.out.println("In Dotty");
            timer = new Timer(40, (ActionEvent e) -> {
                i++;
                if (i < results.size()) {
                    double xd = 10 * results.get(i)[0];
                    double yd = 10 * results.get(i)[2];
                    int x1 = (int) xd;
                    int y1 = (int) yd;
                    System.out.println(x1);
                    Graphics2D g2d = background.createGraphics();
                    g2d.setColor(Color.BLACK);
                    g2d.drawRect(x1, y1, 1, 1);
                    g2d.dispose();
                    repaint();
                } else {
                    ((Timer) e.getSource()).stop();
                }
            });
            timer.setRepeats(true);
            timer.setCoalesce(true);
            timer.start();
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(600, 600);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g.create();
            int x = (getWidth() - background.getWidth()) / 2;
            int y = (getHeight() - background.getHeight()) / 2;
            g2d.drawImage(background, x, y, this);
            g2d.dispose();
        }
    }
}
