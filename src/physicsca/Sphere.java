/*
 * Class to model the object
 */
package physicsca;

/**
 *
 * @author James Farrell and Kristine Saroka
 */
public class Sphere {

    //Doubles
    private double radius;
    private double density;
    private double volume;
    private double mass;
    private double frontalArea;

    //Vectors
    private double[] position;
    private double[] velocity;
    private double[] apparentVelocity;
    private double[] acceleration;
    private double[] angular;

    public Sphere() {
    }

    public Sphere(double radius, double density, double[] position, double[] velocity, double[] angular) {
        this.radius = radius;
        this.density = density;
        this.volume = (4.0 * Math.PI * Math.pow(this.radius, 3)) / 3.0;
        this.mass = this.volume * this.density;
        this.frontalArea = Math.PI * Math.pow(radius, 2);
        this.position = position;
        this.velocity = velocity;
        this.apparentVelocity = new double[3];
        this.acceleration = new double[]{0, 0, 0};
        this.angular = angular;          
    }

    public double getRadius() {
        return radius;
    }

    public double getDensity() {
        return density;
    }

    public double getVolume() {
        return volume;
    }

    public double getMass() {
        return mass;
    }

    public double getFrontalArea() {
        return frontalArea;
    }

    public double[] getPosition() {
        return position;
    }

    public double getPosition(int index) {
        return position[index];
    }

    public double[] getVelocity() {
        return velocity;
    }

    public double[] getApparentVelocity() {
        return this.apparentVelocity;
    }

    public double[] getAcceleration() {
        return acceleration;
    }

    public double[] getAngular() {
        return angular;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public void setPosition(double[] position) {
        this.position = position;
    }

    public void setVelocity(double[] velocity) {
        this.velocity = velocity;
    }

    public void setApparentVelocity(double[] apparentVelocity) {
        this.apparentVelocity = apparentVelocity;
    }

    public void setAcceleration(double[] acceleration) {
        this.acceleration = acceleration;
    }

    public void setAngular(double[] angular) {
        this.angular = angular;
    }

    /**
     * Method to calculate the volume of a sphere Volume: 4/3πr^2
     *
     * @param radius
     * @return
     */
    public double calculateVolume(double radius) {
        return (4.0 / 3) * Math.PI * Math.pow(radius, 3);
    }

    /**
     * Method to calculate the frontal area of a sphere FrontalArea: πr^2
     *
     * @param radius
     * @return
     */
    public double calculateFrontalArea(double radius) {
        return Math.PI * Math.pow(radius, 3);
    }

    /**
     * Method to calculate mass of a sphere: Mass = Force/Acceleration
     *
     * @param force
     * @param acceleration
     * @return 
     */
    public double calculateMass(double force, double acceleration) {
        return force / acceleration;
    }
}
